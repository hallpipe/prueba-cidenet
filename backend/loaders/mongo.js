const mongoose = require('mongoose');

const dbConnection = async () => {
  try {
    // let uri = `mongodb://${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;
    let uri = process.env.DB_CNN;
    await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    console.log('DB online');
  } catch (error) {
    console.log(error);
    throw new Error('Error a la hora de iniciializar la base de datos');
  }
};

module.exports = {
  dbConnection,
};
