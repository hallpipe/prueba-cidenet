const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const { dbConnection } = require('./mongo');

const configServer = ({ app }) => {
  dotenv.config({ path: '.env' });
  dbConnection();

  app.use(cors());

  //Directorio Publico
  //app.use(express.static('public'));

  app.use(express.json());

  app.use('/api/employes', require('../routes/employes'));
  app.use('/api/area', require('../routes/area'));
  app.use('/api/identificactions', require('../routes/identificacion'));

  //app.use('/api/auth', require('./routes/auth'));
  //app.use('/api/events', require('./routes/events'));
};

module.exports = configServer;
