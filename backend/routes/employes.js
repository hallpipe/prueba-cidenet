const { Router } = require('express');
const { check } = require('express-validator');
const {
  createEmployes,
  getEmployes,
  updateEmployed,
  deleteEmployed,
  filterEmployes,
} = require('../controllers/employes');
const {
  validateName,
  validateOtherName,
  validateIdentification,
  validateDateEntry,
} = require('../helpers/validations');
const { validateFields } = require('../middlewares/validar-campos');

const router = Router();

router.post(
  '/create',
  [
    check(
      'firstSurname',
      'El primer apellido no debe tener acentos ni la letra ñ, maximo 20 caracteres y solo letras mayusculas'
    ).custom(validateName),
    check(
      'secondSurname',
      'El segundo apellido no debe tener acentos ni la letra ñ, maximo 20 caracteres y solo letras mayusculas'
    ).custom(validateName),
    check(
      'firstName',
      'El primer nombre no debe tener acentos ni la letra ñ, maximo 20 caracteres y solo letras mayusculas'
    ).custom(validateName),
    check(
      'otherName',
      'El segundo nombre no debe tener acentos ni la letra ñ, maximo 50 caracteres y solo letras mayusculas'
    ).custom(validateOtherName),
    check(
      'numberIdentification',
      'Solo caracteres alfanumericos, maximo 20 caracteres'
    ).custom(validateIdentification),
    check('dateEntry', 'La fecha no puede ser superior al dia actual').custom(
      validateDateEntry
    ),

    validateFields,
  ],
  createEmployes
);

router.post('/filter', filterEmployes);

router.get('/', getEmployes);

router.put(
  '/update/:id',
  [
    check(
      'firstSurname',
      'El primer apellido no debe tener acentos ni la letra ñ, maximo 20 caracteres y solo letras mayusculas'
    ).custom(validateName),
    check(
      'secondSurname',
      'El segundo apellido no debe tener acentos ni la letra ñ, maximo 20 caracteres y solo letras mayusculas'
    ).custom(validateName),
    check(
      'firstName',
      'El primer nombre no debe tener acentos ni la letra ñ, maximo 20 caracteres y solo letras mayusculas'
    ).custom(validateName),
    check(
      'otherName',
      'El segundo nombre no debe tener acentos ni la letra ñ, maximo 50 caracteres y solo letras mayusculas'
    ).custom(validateOtherName),
    check(
      'numberIdentification',
      'Solo caracteres alfanumericos, maximo 20 caracteres'
    ).custom(validateIdentification),
    check('dateEntry', 'La fecha no puede ser superior al dia actual').custom(
      validateDateEntry
    ),

    validateFields,
  ],
  updateEmployed
);

router.delete('/delete/:id', deleteEmployed);

module.exports = router;
