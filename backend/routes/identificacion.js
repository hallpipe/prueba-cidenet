const { Router } = require('express');
const { check } = require('express-validator');
const {
  createIdentification,
  getIdentifications,
} = require('../controllers/identificacion');

const { validateFields } = require('../middlewares/validar-campos');

const router = Router();

router.post(
  '/create',
  [
    check('key', 'Debe de ir una llave').not().isEmpty(),
    check('value', 'Debe de ir un valor').not().isEmpty(),

    validateFields,
  ],
  createIdentification
);

router.get('/', getIdentifications);

module.exports = router;
