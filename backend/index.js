const configServer = require('./loaders/express');
const express = require('express');

const startServer = () => {
  const app = express();

  configServer({ app });

  app.listen(process.env.PORT, () => {
    console.log(`Servidor corriendo en puerto ${process.env.PORT}`);
  });
};

startServer();
