const { Schema, model } = require('mongoose');

const AreaSchema = Schema({
  key: {
    type: String,
    uniqued: true,
  },
  value: {
    type: String,
    uniqued: true,
  },
});

module.exports = model('Area', AreaSchema);
