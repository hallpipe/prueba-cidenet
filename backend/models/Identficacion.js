const { Schema, model } = require('mongoose');

const IdentificacionSchema = Schema({
  key: {
    type: String,
  },
  value: {
    type: String,
  },
});

module.exports = model('Identificacion', IdentificacionSchema);
