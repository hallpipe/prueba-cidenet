const { Schema, model } = require('mongoose');

const EmployeSchema = Schema({
  firstSurname: {
    type: String,
    required: true,
  },
  secondSurname: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  otherName: {
    type: String,
  },
  country: {
    type: String,
  },
  typeIdentification: {
    type: Schema.Types.ObjectId,
    ref: 'Identificacion',
    required: true,
  },
  numberIdentification: {
    type: String,
    unique: true,
  },
  email: {
    type: String,
    required: true,
  },
  dateEntry: {
    type: Date,
  },
  area: {
    type: Schema.Types.ObjectId,
    ref: 'Area',
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  registrationDate: {
    type: Date,
  },
});

EmployeSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

module.exports = model('Empleados', EmployeSchema);
