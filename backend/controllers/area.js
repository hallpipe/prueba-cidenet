const { response } = require('express');
const Area = require('../models/Area');

const createArea = async (req, res = response) => {
  try {
    const existArea = await Area.find({ key: req.key });

    if (!existArea.length) {
      const area = new Area(req.body);
      await area.save();
      res.json({ ok: true, area });
    } else {
      return res
        .status(400)
        .json({ ok: false, msg: 'Ya existe en la base de datos' });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

const getAreas = async (req, res = response) => {
  try {
    const areas = await Area.find({});
    res.json({ ok: true, areas });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

module.exports = {
  createArea,
  getAreas,
};
