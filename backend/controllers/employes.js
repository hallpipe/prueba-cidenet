const { response } = require('express');
const Employes = require('../models/Employes');
const { generateEmail } = require('../helpers/email');

const getEmployes = async (req, res = response) => {
  try {
    const employes = await Employes.find({})
      .populate('typeIdentification', 'value')
      .populate('area', 'value')
      .exec();

    res.json({ ok: true, employes });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

const filterEmployes = async (req, res = response) => {
  try {
    const employes = await Employes.find(req.body)
      .populate('typeIdentification', 'value')
      .populate('area', 'value')
      .exec();

    res.json({ ok: true, employes });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

const createEmployes = async (req, res = response) => {
  try {
    const { firstName, firstSurname, country } = req.body;

    let email = await generateEmail(firstName, firstSurname, country);

    let newObject = {
      ...req.body,
      status: 'ACTIVO',
      email,
      registrationDate: new Date(),
    };
    const employ = new Employes(newObject);

    await employ.save();
    res.json({ ok: true, employ });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

const updateEmployed = async (req, res = response) => {
  const employedId = req.params.id;

  try {
    const employ = await Employes.findById(employedId);

    if (!employ) {
      return res
        .status(400)
        .json({ ok: false, msg: 'El empleado no existe no existe con ese id' });
    }

    let email = await generateEmail(
      req.body.firstName,
      req.body.firstSurname,
      req.body.country
    );

    const dataToUpdated = {
      ...req.body,
      email,
    };

    const dataUpdated = await Employes.findByIdAndUpdate(
      employedId,
      dataToUpdated,
      { new: true }
    );

    res.json({ ok: true, employ: dataUpdated });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: false, msg: 'Hable con el administrador' });
  }
};

const deleteEmployed = async (req, res = response) => {
  const employedId = req.params.id;
  const uid = req.uid;

  try {
    const employed = await Employes.findById(employedId);

    if (!employed) {
      return res
        .status(400)
        .json({ ok: false, msg: 'El empleado no existe no existe con ese id' });
    }

    await Employes.findByIdAndDelete(employedId);

    res.json({ ok: true });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: false, msg: 'Hable con el administrador' });
  }
};

module.exports = {
  createEmployes,
  getEmployes,
  updateEmployed,
  deleteEmployed,
  filterEmployes,
};
