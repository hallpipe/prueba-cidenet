const { response } = require('express');
const Identificacion = require('../models/Identficacion');

const createIdentification = async (req, res = response) => {
  try {
    const existIdentification = await Identificacion.find({ key: req.key });

    if (!existIdentification.length) {
      const identification = new Identificacion(req.body);
      await identification.save();
      res.json({ ok: true, identification });
    } else {
      return res
        .status(400)
        .json({ ok: false, msg: 'Ya existe en la base de datos' });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

const getIdentifications = async (req, res = response) => {
  try {
    const identifications = await Identificacion.find({});
    res.json({ ok: true, identifications });
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: true, msg: 'Hable con el administrador' });
  }
};

module.exports = {
  createIdentification,
  getIdentifications,
};
