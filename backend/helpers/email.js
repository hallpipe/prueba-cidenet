const Employes = require('../models/Employes');

const generateEmail = async (firstName, firstSurname, country) => {
  if (/\s/.test(firstSurname)) {
    firstSurname = firstSurname.toLowerCase().replace(/\s/, '');
  }

  let domain = country === 'Colombia' ? '@cidenet.com.co' : '@cidenet.com.us';
  let email = `${firstName.toLowerCase()}.${firstSurname.toLowerCase()}${domain}`;

  let employes = await Employes.find({
    email: {
      $regex: `${firstName.toLowerCase()}.${firstSurname.toLowerCase()}`,
    },
  }).exec();
  if (employes && employes.length) {
    email = `${firstName.toLowerCase()}.${firstSurname.toLowerCase()}.${
      employes.length + 1
    }${domain}`;
  }

  return email;
};

module.exports = {
  generateEmail,
};
