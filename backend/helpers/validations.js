const moment = require('moment');

const validateName = (value) => {
  if (
    value.match(/^[A-Z\s]*$/) &&
    !value.match(/^[ÁÉÍÓÚáéíóúñÑ]*$/) &&
    value.length <= 20
  ) {
    return true;
  }
};

const validateOtherName = (value) => {
  if (!value) {
    return true;
  }
  if (
    value.match(/^[A-Z\s]*$/) &&
    !value.match(/^[ÁÉÍÓÚáéíóúñÑ]*$/) &&
    value.length <= 50
  ) {
    return true;
  }
};

const validateIdentification = (value) => {
  if (value.match(/^[A-Za-z0-9-]*$/)) {
    return true;
  }
};

const validateDateEntry = (value) => {
  const fecha = moment(value);
  let today = moment();

  let days = fecha.diff(today, 'days');

  if (days >= 0) {
    return false;
  } else {
    return true;
  }
};

module.exports = {
  validateName,
  validateOtherName,
  validateIdentification,
  validateDateEntry,
};
