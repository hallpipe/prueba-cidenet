import React, { useContext, useState } from 'react';
import Swal from 'sweetalert2';
import { CedinetContext } from '../../context/Context';
import { deleteEmploy, filterEmployes } from '../../http/employes';
import { ModalEmploye } from './ModalEmploye';

import { DetailEmploye } from './DetailEmploye';
import { formatFilters } from '../../utils/utils';
import { CreateArea } from '../area/CreateArea';
import { CreateIdentification } from '../identification/CreateIdentification';

export const ListEmployes = () => {
  const [filters, setFilters] = useState(null);
  const [isFilter, setIsFilter] = useState(false);
  const {
    setModalOpen,
    setInfoEmploy,
    state,
    dispatch,
    setModalOpenCreate,
    setIsEdit,
    setEmploy,
  } = useContext(CedinetContext);

  const deleteEmployed = async (result, id) => {
    if (result.isConfirmed) {
      let { data } = await deleteEmploy(id);
      if (data.ok) {
        Swal.fire('Eliminado', 'Empleado Eliminado.', 'success');
        dispatch({ type: 'DELETE_EMPLOYEE', payload: id });
      } else {
        Swal.fire('Error', 'Ocurrio un error eliminando el empleado', 'error');
      }
    }
  };

  const setfilterEmployes = ({ target }) => {
    setFilters({
      ...filters,
      [target.name]: target.value,
    });
  };
  return (
    <div className="container container-fluid">
      <div className="row">
        <div className="col-12">
          <div className="table-responsive">
            {state?.employes ? (
              <table className="table">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">
                      Primer Nombre
                      <select
                        className="form form-control"
                        onChange={setfilterEmployes}
                        name="firstName"
                        data-testid="filterName"
                        value={filters ? filters?.firstName : 'option-0'}
                      >
                        <option disabled value={'option-0'}>
                          Seleccione ...
                        </option>

                        {formatFilters(state.employes, 'firstName').map(
                          (firstName, index) => (
                            <option
                              key={`${firstName}-${index}`}
                              value={firstName}
                            >
                              {firstName}
                            </option>
                          )
                        )}
                      </select>
                    </th>
                    <th scope="col">
                      Otros Nombres
                      <select
                        className="form form-control"
                        name="otherName"
                        onChange={setfilterEmployes}
                        value={filters ? filters?.otherName : 'option-0'}
                      >
                        <option selected disabled value="option-0">
                          Seleccione ...
                        </option>

                        {formatFilters(state.employes, 'otherName').map(
                          (otherName, index) => (
                            <option
                              key={`${otherName}-${index}`}
                              value={otherName}
                            >
                              {otherName}
                            </option>
                          )
                        )}
                      </select>
                    </th>
                    <th scope="col">
                      Primer Apellido
                      <select
                        className="form form-control"
                        name="firstSurname"
                        onChange={setfilterEmployes}
                        value={filters ? filters?.firstSurname : 'option-0'}
                      >
                        <option selected disabled value="option-0">
                          Seleccione ...
                        </option>

                        {formatFilters(state.employes, 'firstSurname').map(
                          (firstSurname, index) => (
                            <option
                              key={`${firstSurname}-${index}`}
                              value={firstSurname}
                            >
                              {firstSurname}
                            </option>
                          )
                        )}
                      </select>
                    </th>
                    <th scope="col">
                      Segundo Apellido
                      <select
                        className="form form-control"
                        name="secondSurname"
                        onChange={setfilterEmployes}
                        value={filters ? filters?.secondSurname : 'option-0'}
                      >
                        <option selected disabled value="option-0">
                          Seleccione ...
                        </option>
                        {formatFilters(state.employes, 'secondSurname').map(
                          (secondSurname, index) => (
                            <option
                              key={`${secondSurname}-${index}`}
                              value={secondSurname}
                            >
                              {secondSurname}
                            </option>
                          )
                        )}
                      </select>
                    </th>
                    <th scope="col">
                      Pais
                      <select
                        className="form form-control"
                        name="country"
                        onChange={setfilterEmployes}
                        value={filters ? filters?.country : 'option-0'}
                      >
                        <option selected disabled value="option-0">
                          Seleccione ...
                        </option>
                        {formatFilters(state.employes, 'country').map(
                          (country) => (
                            <option key={`co-${country}`} value={country}>
                              {country}
                            </option>
                          )
                        )}
                      </select>
                    </th>
                    <th scope="col">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {state?.employes.map((employ) => (
                    <tr key={employ.id}>
                      <td data-testid="name">{employ.firstName}</td>
                      <td>{employ.otherName}</td>
                      <td>{employ.firstSurname}</td>
                      <td>{employ.secondSurname}</td>
                      <td>{employ.country}</td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-primary btn-sm"
                          data-testid="view-detail"
                          onClick={() => {
                            setInfoEmploy({
                              typeIdentification: employ.typeIdentification,
                              numberIdentification: employ.numberIdentification,
                              dateEntry: employ.dateEntry,
                              registrationDate: employ.registrationDate,
                              area: employ.area,
                              email: employ.email,
                            });
                            setModalOpen(true);
                          }}
                        >
                          <i className="far fa-eye"></i>
                        </button>
                        <button
                          type="button"
                          data-testid="edit"
                          className="btn btn-success btn-sm"
                          onClick={() => {
                            setEmploy({
                              firstName: employ.firstName,
                              otherName: employ.otherName,
                              firstSurname: employ.firstSurname,
                              secondSurname: employ.secondSurname,
                              country: employ.country,
                              typeIdentification: employ.typeIdentification._id,
                              numberIdentification: employ.numberIdentification,
                              dateEntry: employ.dateEntry,
                              area: employ.area._id,
                              id: employ.id,
                            });

                            setModalOpenCreate(true);
                            setIsEdit(true);
                          }}
                        >
                          <i className="fas fa-edit"></i>
                        </button>
                        <button
                          type="button"
                          className="btn btn-danger btn-sm"
                          data-testid="delete"
                          onClick={() =>
                            Swal.fire({
                              title: 'Esta seguro?',
                              text: 'Esta acción es irreversible!',
                              icon: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Si eliminar!',
                              cancelButtonText: 'Cancelar',
                            }).then((result) => {
                              deleteEmployed(result, employ.id);
                            })
                          }
                        >
                          <i className="far fa-trash-alt"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            ) : null}
          </div>
        </div>

        <div className="col">
          {filters != null && (
            <button
              type="button"
              data-testid="btn-filtrar"
              className="btn btn-success"
              onClick={async () => {
                setIsFilter(true);
                let { data } = await filterEmployes(filters);
                dispatch({ type: 'GET_EMPLOYES', payload: data.employes });
              }}
            >
              Filtrar
            </button>
          )}

          {isFilter ? (
            <button
              type="button"
              className="btn btn-warning"
              data-testid="btn-cleanFilters"
              onClick={async () => {
                setFilters(null);
                setIsFilter(false);
                let { data } = await filterEmployes({});
                dispatch({ type: 'GET_EMPLOYES', payload: data.employes });
              }}
            >
              Limpiar
            </button>
          ) : null}
          <button
            type="button"
            data-testid="btn-new"
            className="btn btn-info"
            onClick={() => {
              setModalOpenCreate(true);
              setIsEdit(false);
            }}
          >
            Crear Empleado
          </button>
        </div>
        <div className="col">
          <CreateArea />
        </div>

        <div className="col">
          <CreateIdentification />
        </div>
      </div>
      <DetailEmploye />
      <ModalEmploye />
    </div>
  );
};
