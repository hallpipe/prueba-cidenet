import React, { useContext } from 'react';
import Modal from 'react-modal';
import { CedinetContext } from '../../context/Context';

export const DetailEmploye = () => {
  Modal.setAppElement('#root');
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  const { modalOpen, setModalOpen, infoEmploy, setInfoEmploy } =
    useContext(CedinetContext);
  const {
    typeIdentification,
    numberIdentification,
    dateEntry,
    registrationDate,
    area,
    email,
  } = infoEmploy;

  const closeModal = () => {
    setModalOpen(false);
    setInfoEmploy({});
  };

  return (
    <Modal
      isOpen={modalOpen}
      style={customStyles}
      closeTimeoutMS={200}
      classNameName="modal"
      overlayclassNameName="modal-fondo"
    >
      <div className="container">
        <div className="card" style={{ width: '25rem' }}>
          <div className="card-body">
            <h5 className="card-title">Información</h5>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                Tipo de identificación: {typeIdentification?.value}
              </li>
              <li className="list-group-item">
                Nro Identificacion: {numberIdentification}
              </li>
              <li className="list-group-item">
                Fecha de Ingreso: {dateEntry?.split('T')[0]}
              </li>
              <li className="list-group-item">
                Fecha de Registro: {registrationDate?.split('T')[0]}
              </li>
              <li className="list-group-item">Area: {area?.value}</li>
              <li className="list-group-item">Email: {email}</li>
            </ul>

            <button className="btn btn-warning" onClick={closeModal}>
              Cerrar
            </button>
          </div>
        </div>
      </div>
    </Modal>
  );
};
