import React, { useContext, useEffect, useState, useRef } from 'react';
import Modal from 'react-modal';
import Swal from 'sweetalert2';
import { CedinetContext } from '../../context/Context';
import { useForm } from '../../hooks/userForm';

import { createEmployes, editEmployes } from '../../http/employes';

import { validateForm } from '../../utils/utils';

export const ModalEmploye = () => {
  const [validations, setValidations] = useState(null);
  Modal.setAppElement('#root');
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: '50%',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  const {
    modalOpenCreate,
    setModalOpenCreate,
    dispatch,
    isEdit,
    employ,
    setEmploy,
    stateAreas,
    stateIdentifications,
  } = useContext(CedinetContext);

  const [values, handleInputChange, reset] = useForm(employ);

  const activeId = useRef(employ.id);

  useEffect(() => {
    if (employ.id !== activeId.current) {
      reset(employ);
      activeId.current = employ.id;
    }
  }, [employ, reset]);

  const {
    firstName,
    otherName,
    firstSurname,
    secondSurname,
    country,
    typeIdentification,
    numberIdentification,
    dateEntry,
    area,
  } = values;

  const handleCreateEmployee = async (e) => {
    e.preventDefault();

    try {
      let { validations, validation } = validateForm(values);
      setValidations(validations);

      if (validation) {
        if (isEdit) {
          let { data } = await editEmployes(employ.id, values);
          closeModal(validations);
          dispatch({
            type: 'EDIT_EMPLOYE',
            payload: { id: employ.id, employ: data.employ },
          });
        } else {
          let { data } = await createEmployes(values);
          closeModal(validations);
          dispatch({ type: 'ADD_EMPLOYE', payload: data.employ });
        }
      }
    } catch (error) {
      Swal.fire('Error', 'por favor comuniquese con el adminsitrador', 'error');
    }
  };

  const closeModal = (validations?) => {
    reset();
    setModalOpenCreate(false);
    setValidations(null);
    setEmploy({
      firstName: '',
      otherName: '',
      firstSurname: '',
      secondSurname: '',
      country: '',
      typeIdentification: '',
      numberIdentification: '',
      dateEntry: '',
      area: '',
    });
  };

  return (
    <Modal
      isOpen={modalOpenCreate}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={200}
    >
      <div className="container">
        <h1>{isEdit ? `Editar Empleado` : 'Nuevo Empleado'} </h1>

        <form className="container" onSubmit={handleCreateEmployee}>
          <div className="row">
            <div className="form-group col-sm-6">
              <label>Primer Nombre</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="Nombre"
                autoComplete="off"
                name="firstName"
                maxLength={20}
                value={firstName}
                onChange={handleInputChange}
                aria-describedby="passwordHelpBlock"
              />
              {validations?.firstName ? (
                <div className="alert-red">
                  El primer nombre no debe tener acentos ni la letra ñ, maximo
                  20 caracteres y solo letras mayusculas
                </div>
              ) : null}
            </div>

            <div className="form-group col-sm-6">
              <label>Otros Nombres</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="Otros Nombre"
                autoComplete="off"
                maxLength={50}
                name="otherName"
                value={otherName}
                onChange={handleInputChange}
              />

              {validations?.otherName ? (
                <div className="alert-red">
                  El segundo nombre no debe tener acentos ni la letra ñ, maximo
                  50 caracteres y solo letras mayusculas
                </div>
              ) : null}
            </div>
          </div>
          <div className="row">
            <div className="form-group col-sm-6">
              <label>Primer Apellido</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="Primer Apellido"
                autoComplete="off"
                maxLength={20}
                name="firstSurname"
                value={firstSurname}
                onChange={handleInputChange}
              />

              {validations?.firstSurname ? (
                <div className="alert-red">
                  El primer apellido no debe tener acentos ni la letra ñ, maximo
                  20 caracteres y solo letras mayusculas
                </div>
              ) : null}
            </div>
            <div className="form-group col-sm-6">
              <label>Segundo Apellido</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="Segundo Apellido"
                name="secondSurname"
                value={secondSurname}
                autoComplete="off"
                onChange={handleInputChange}
              />

              {validations?.secondSurname ? (
                <div className="alert-red">
                  El segundo apellido no debe tener acentos ni la letra ñ,
                  maximo 20 caracteres y solo letras mayusculas
                </div>
              ) : null}
            </div>
          </div>

          <div className="row">
            <div className="form-group col-sm-6">
              <label>Pais</label>
              <select
                className={`form-control`}
                name="country"
                onChange={handleInputChange}
                value={country}
              >
                <option value="" key="0-pais" disabled>
                  Seleccione un pais
                </option>
                <option value="Colombia" key="colombia">
                  Colombia
                </option>
                <option value="EEUU" key="eeuu">
                  Estados Unidos
                </option>
              </select>
            </div>

            <div className="form-group col-sm-6">
              <label>Tipo de Identificación</label>
              <select
                className={`form-control`}
                name="typeIdentification"
                onChange={handleInputChange}
                value={typeIdentification}
              >
                <option value="" key="0-ti" disabled>
                  Seleccione un tipo de identificación
                </option>
                {stateIdentifications &&
                stateIdentifications.identifications &&
                stateIdentifications.identifications.length
                  ? stateIdentifications.identifications.map(
                      (identifications) => (
                        <option
                          key={identifications.key}
                          value={identifications._id}
                        >
                          {identifications.value}
                        </option>
                      )
                    )
                  : null}
              </select>
            </div>
          </div>

          <div className="row">
            <div className="form-group col-sm-6">
              <label>Número Identificación</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="Número de Identificación"
                name="numberIdentification"
                maxLength={20}
                onChange={handleInputChange}
                value={numberIdentification}
                autoComplete="off"
              />

              {validations?.numberIdentification ? (
                <div className="alert-red">
                  Solo caracteres alfanumericos, maximo 20 caracteres
                </div>
              ) : null}
            </div>

            <div className="form-group col-sm-6">
              <label>Fecha de Ingreso</label>
              <input
                type="date"
                className={`form-control`}
                placeholder="Fecha de Ingreso"
                name="dateEntry"
                onChange={handleInputChange}
                value={dateEntry}
                autoComplete="off"
              />
              {validations?.dateEntry ? (
                <div className="alert-red">
                  La fecha no puede ser superior al dia actual
                </div>
              ) : null}
            </div>
          </div>

          <div className="row">
            <div className="form-group col-sm-6 flex-column d-flex">
              <label>Area</label>

              <select
                className={`form-control`}
                name="area"
                onChange={handleInputChange}
                value={area}
              >
                <option value="" key="0-area" disabled>
                  Seleccione un Area
                </option>

                {console.log(stateAreas)}
                {stateAreas && stateAreas.areas && stateAreas.areas.length
                  ? stateAreas.areas.map((area) => (
                      <option key={area.key} value={area._id}>
                        {area.value}
                      </option>
                    ))
                  : null}
              </select>
            </div>
          </div>

          <button type="submit" className="btn btn-outline-primary btn-block">
            <i className="far fa-save"></i>
            <span> Guardar</span>
          </button>
        </form>
      </div>
    </Modal>
  );
};
