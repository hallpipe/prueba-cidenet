import { fireEvent, render } from '@testing-library/react';
import { DetailEmploye } from '../DetailEmploye';
import { CedinetContext } from '../../../context/Context';

describe('Test in Detail Employe', () => {
  let element = null;
  let component = null;
  let openModal = jest.fn();
  beforeEach(() => {
    element = document.createElement('div');
    element.setAttribute('id', 'root');
    document.body.appendChild(element);
    component = render(
      <CedinetContext.Provider
        value={{
          modalOpen: true,
          setModalOpen: openModal,
          infoEmploy: {
            typeIdentification: { value: 'Cedula' },
            numberIdentification: '103664899',
            dateEntry: '2020-10-10',
            registrationDate: '2020-10-11',
            area: { value: 'Adminsitrativo' },
            email: 'adiaz@pratechgroup.com',
          },
          setInfoEmploy: jest.fn(),
        }}
      >
        <DetailEmploye />
      </CedinetContext.Provider>
    );
  });
  test('should render the component and type identification is Cedula', () => {
    let { getByText } = component;
    let text = getByText(/Tipo de identificación:/);
    expect(text).toHaveTextContent('Cedula');
  });

  test('should render the component and close Modal', () => {
    let { getByRole } = component;
    let button = getByRole('button', {
      name: /Cerrar/i,
    });

    fireEvent.click(button);
    expect(openModal).toHaveBeenCalledWith(false);
  });
});
