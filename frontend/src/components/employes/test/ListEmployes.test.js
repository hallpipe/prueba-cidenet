import { fireEvent, render } from '@testing-library/react';
import { ListEmployes } from '../ListEmployes';
import { CedinetContext } from '../../../context/Context';
import axios from 'axios';
import Swal from 'sweetalert2';
jest.mock('axios');

describe('Test in List Employes', () => {
  console.error = jest.fn();
  let element = null;
  let component = render();

  let setModalOpen = jest.fn();
  let setIsEdit = jest.fn();
  let setModalOpenCreate = jest.fn();
  let dispatch = jest.fn();
  beforeEach(() => {
    element = document.createElement('div');
    element.setAttribute('id', 'root');
    document.body.appendChild(element);
    component = render(
      <CedinetContext.Provider
        value={{
          modalOpen: true,
          setModalOpen,
          state: {
            employes: [
              {
                id: '60ee7d6b5b6a4d4df83094b5',
                firstSurname: 'DIAZ ZAPATA',
                secondSurname: 'ZAPATA',
                firstName: 'ANDRES',
                otherName: 'ANDRES DIAZ',
                country: 'Colombia',
                typeIdentification: '60ee7b7259881d4ea06f89ee',
                numberIdentification: 'C1036648996',
                dateEntry: '2021-07-11T00:00:00.000Z',
                area: '60ee79c61351761b74064cb7',
                status: 'ACTIVO',
                email: 'andres.diazzapata@cidenet.com.co',
                registrationDate: '2021-07-14T06:00:11.130Z',
              },
            ],
          },

          employ: {
            id: '60ee7d6b5b6a4d4df83094b5',
            firstSurname: 'DIAZ ZAPATA',
            secondSurname: 'ZAPATA',
            firstName: 'ANDRES',
            otherName: 'ANDRES DIAZ',
            country: 'Colombia',
            typeIdentification: '60ee7b7259881d4ea06f89ee',
            numberIdentification: 'C1036648996',
            dateEntry: '2021-07-11T00:00:00.000Z',
            area: '60ee79c61351761b74064cb7',
            status: 'ACTIVO',
            email: 'andres.diazzapata@cidenet.com.co',
            registrationDate: '2021-07-14T06:00:11.130Z',
          },
          infoEmploy: {
            typeIdentification: { value: 'Cedula' },
            numberIdentification: '103664899',
            dateEntry: '2020-10-10',
            registrationDate: '2020-10-11',
            area: { value: 'Adminsitrativo' },
            email: 'adiaz@pratechgroup.com',
          },
          setInfoEmploy: jest.fn(),
          setModalOpenCreate,
          setIsEdit,
          setEmploy: jest.fn(),
          dispatch,
        }}
      >
        <ListEmployes />
      </CedinetContext.Provider>
    );
  });

  test('should render component and show name ANDRES', () => {
    let { getByTestId } = component;
    expect(getByTestId('name')).toHaveTextContent('ANDRES');
  });

  test('should render component and click in show detail', () => {
    let { getByTestId } = component;
    let button = getByTestId('view-detail');

    fireEvent.click(button);
    expect(setModalOpen).toHaveBeenCalledWith(true);
  });

  test('should render component and click in edit', () => {
    let { getByTestId } = component;
    let button = getByTestId('edit');

    fireEvent.click(button);
    expect(setModalOpenCreate).toHaveBeenCalledWith(true);
    expect(setIsEdit).toHaveBeenCalledWith(true);
  });

  test('should render component and click in delete and user delete success', () => {
    let { getByTestId } = component;
    Swal.fire = jest.fn().mockResolvedValue({ isConfirmed: true });
    axios.delete.mockResolvedValue({ data: { ok: true } });
    let button = getByTestId('delete');
    fireEvent.click(button);
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  test('should render component and click in delete and user delete error', () => {
    let { getByTestId } = component;
    Swal.fire = jest.fn().mockResolvedValue({ isConfirmed: true });
    axios.delete.mockResolvedValue({ data: { ok: false } });
    let button = getByTestId('delete');
    fireEvent.click(button);
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  test('should render component and click button filter', () => {
    let { getByTestId } = component;
    axios.post.mockResolvedValue({ data: { employes: [] } });
    let selectFilter = getByTestId('filterName');
    fireEvent.change(selectFilter, { name: 'firstName', value: 'Andres' });

    let buttonFilter = getByTestId('btn-filtrar');
    fireEvent.click(buttonFilter);
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  test('should render component and click clean filter', () => {
    let { getByTestId } = component;
    axios.post.mockResolvedValue({ data: { employes: [] } });
    let selectFilter = getByTestId('filterName');
    fireEvent.change(selectFilter, { name: 'firstName', value: 'Andres' });

    let buttonFilter = getByTestId('btn-filtrar');
    fireEvent.click(buttonFilter);
    let buttonCleanFilter = getByTestId('btn-cleanFilters');
    fireEvent.click(buttonCleanFilter);
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  test('should render component and click in create employe', () => {
    let { getByTestId } = component;
    axios.post.mockResolvedValue({ data: { employes: [] } });

    let buttonCleanFilter = getByTestId('btn-new');
    fireEvent.click(buttonCleanFilter);
    expect(setModalOpenCreate).toHaveBeenCalledWith(true);
    expect(setIsEdit).toHaveBeenCalledWith(false);
  });
});
