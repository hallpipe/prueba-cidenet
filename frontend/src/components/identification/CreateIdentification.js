import React, { useContext, useState } from 'react';
import Modal from 'react-modal';
import Swal from 'sweetalert2';
import { CedinetContext } from '../../context/Context';
import { useForm } from '../../hooks/userForm';
import { createIdentifications } from '../../http/identificaciones';

export const CreateIdentification = ({ title, type }) => {
  const [openModal, setModalOpen] = useState(false);
  const { dispatchIdentifications } = useContext(CedinetContext);

  const [values, handleInputChange, reset] = useForm({ key: '', value: '' });

  const { key, value } = values;

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: '50%',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const handleCreateArea = async (e) => {
    e.preventDefault();
    try {
      if (key.length && value.length) {
        let { data } = await createIdentifications(values);
        dispatchIdentifications({
          type: 'ADD_IDENTIFICATION',
          payload: data.identification,
        });
        closeModal();
        reset();
      } else {
        Swal.fire('Error', 'Revise los campos y vuelva a intentarlo', 'error');
      }
    } catch (error) {
      if (error.status === 400) {
        Swal.fire('Error', error.data.msg, 'error');
      } else {
        Swal.fire(
          'Error',
          'por favor comuniquese con el adminisitrador',
          'error'
        );
      }
    }
  };

  return (
    <div>
      <button
        type="button"
        className="btn btn-success"
        onClick={() => {
          setModalOpen(true);
        }}
      >
        Crear Identificaciones
      </button>

      <Modal
        isOpen={openModal}
        onRequestClose={closeModal}
        style={customStyles}
        closeTimeoutMS={200}
      >
        <form className="container" onSubmit={handleCreateArea}>
          <h1>Crea Identificaciones</h1>
          <div className="row">
            <div className="form-group col-sm-6">
              <label>Llave</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="LLave"
                autoComplete="off"
                name="key"
                maxLength={20}
                value={key}
                onChange={handleInputChange}
              />
            </div>

            <div className="form-group col-sm-6">
              <label>Valor</label>
              <input
                type="text"
                className={`form-control`}
                placeholder="Valor"
                autoComplete="off"
                maxLength={50}
                name="value"
                value={value}
                onChange={handleInputChange}
              />
            </div>
          </div>

          <button type="submit" className="btn btn-outline-primary btn-block">
            <i className="far fa-save"></i>
            <span> Guardar</span>
          </button>
        </form>
      </Modal>
    </div>
  );
};
