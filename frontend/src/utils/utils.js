const moment = require('moment');

export const formatFilters = (values, property) => {
  let filters = values.map((e) => e[property]).filter(Boolean);
  let newArray = [...new Set(filters)];
  return newArray;
};

export const validateForm = (values) => {
  let keys = Object.keys(values);

  let validations = {};
  let validation = true;

  for (const key of keys) {
    switch (key) {
      case 'firstSurname':
      case 'secondSurname':
      case 'firstName':
        if (
          !/^[A-Z\s]*$/.test(values[key]) ||
          /^[ÁÉÍÓÚáéíóúñÑ]*$/.test(values[key])
        ) {
          validations[key] = true;
          validation = false;
        } else {
          validations[key] = false;
        }

        break;
      case 'otherName':
        if (values[key] && values[key].length) {
          if (
            !/^[A-Z\s]*$/.test(values[key]) ||
            /^[ÁÉÍÓÚáéíóúñÑ]*$/.test(values[key])
          ) {
            validations[key] = true;
            validation = false;
          } else {
            validations[key] = false;
          }
        } else {
          validations[key] = false;
        }

        break;
      case 'numberIdentification':
        if (!/^[A-Za-z0-9-]*$/.test(values[key])) {
          validations[key] = true;
          validation = false;
        } else {
          validations[key] = false;
        }

        break;
      case 'dateEntry':
        if (values[key] && values[key].length) {
          const fecha = moment(values[key]);
          let today = moment();
          let days = fecha.diff(today, 'days');
          if (days >= 0) {
            validations[key] = true;
            validation = false;
          } else {
            validations[key] = false;
          }
        } else {
          validations[key] = false;
        }

        break;

      default:
    }
  }

  console.log({ validations, validation });

  return { validations, validation };
};
