import { renderHook, act } from '@testing-library/react-hooks';
import { useForm } from '../userForm';

describe('Pruebas en useForm', () => {
  const initialForm = { name: 'Andres', email: 'hallpipe10@gmail.com' };

  test('debe de regresar un formulario por defecto', () => {
    const { result } = renderHook(() => useForm(initialForm));

    const [values, handleInputChange, reset] = result.current;

    expect(values).toEqual(initialForm);
    expect(typeof handleInputChange).toBe('function');
    expect(typeof reset).toBe('function');
  });

  test('debe de rcambiar el valor del formulario cambiar name', () => {
    const { result } = renderHook(() => useForm(initialForm));

    const [, handleInputChange] = result.current;
    const event = { target: { name: 'name', value: 'Felipe' } };

    act(() => {
      handleInputChange(event);
    });

    const [values] = result.current;
    expect(values.name).toBe('Felipe');
  });

  test('Debe de reestablecer el formulario con reset', () => {
    const { result } = renderHook(() => useForm(initialForm));

    const [, handleInputChange, reset] = result.current;
    const event = { target: { name: 'name', value: 'Felipe' } };

    act(() => {
      handleInputChange(event);
      reset();
    });

    const [values] = result.current;
    expect(values.name).toBe('Andres');
  });
});
