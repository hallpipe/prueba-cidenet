const initialState = {
  employes: [],
};

export const employeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_EMPLOYES':
      return {
        ...state,
        employes: action.payload,
      };

    case 'DELETE_EMPLOYEE':
      return {
        ...state,
        employes: state.employes.filter(
          (employ) => employ.id !== action.payload
        ),
      };

    case 'ADD_EMPLOYE':
      return {
        ...state,
        employes: [...state.employes, action.payload],
      };

    case 'EDIT_EMPLOYE':
      return {
        ...state,
        employes: state.employes.map((employ) =>
          employ.id === action.payload.id ? action.payload.employ : employ
        ),
      };

    default:
      return state;
  }
};
