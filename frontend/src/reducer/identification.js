const initialState = {
  identifications: [],
};

export const identificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_IDENTIFICATIONS':
      return {
        ...state,
        identifications: action.payload,
      };

    case 'ADD_IDENTIFICATION':
      return {
        ...state,
        identifications: [...state.identifications, action.payload],
      };

    default:
      return state;
  }
};
