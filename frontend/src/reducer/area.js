const initialState = {
  areas: [],
};

export const areaReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_AREAS':
      return {
        ...state,
        areas: action.payload,
      };

    case 'ADD_AREA':
      return {
        ...state,
        areas: [...state.areas, action.payload],
      };

    default:
      return state;
  }
};
