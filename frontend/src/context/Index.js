import React, { useEffect, useReducer, useState } from 'react';
import { getArea } from '../http/area';
import { getEmployes } from '../http/employes';
import { getIdentifications } from '../http/identificaciones';
import { areaReducer } from '../reducer/area';
import { employeeReducer } from '../reducer/employe';
import { identificationReducer } from '../reducer/identification';
import { CedinetContext } from './Context';

export const IndexProvider = ({ children }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalOpenCreate, setModalOpenCreate] = useState(false);
  const [infoEmploy, setInfoEmploy] = useState({});
  const [state, dispatch] = useReducer(employeeReducer);
  const [stateAreas, dispatchAreas] = useReducer(areaReducer);
  const [stateIdentifications, dispatchIdentifications] = useReducer(
    identificationReducer
  );
  const [isEdit, setIsEdit] = useState(false);
  const [employ, setEmploy] = useState({
    firstName: '',
    otherName: '',
    firstSurname: '',
    secondSurname: '',
    country: '',
    typeIdentification: '',
    numberIdentification: '',
    dateEntry: '',
    area: '',
    id: '',
  });

  useEffect(() => {
    getEmployes().then((resp) => {
      dispatch({ type: 'GET_EMPLOYES', payload: resp.data.employes });
    });
  }, []);

  useEffect(() => {
    getArea().then((area) => {
      dispatchAreas({ type: 'GET_AREAS', payload: area.data.areas });
    });
  }, []);

  useEffect(() => {
    getIdentifications().then((identifications) => {
      dispatchIdentifications({
        type: 'GET_IDENTIFICATIONS',
        payload: identifications.data.identifications,
      });
    });
  }, []);

  return (
    <CedinetContext.Provider
      value={{
        modalOpen,
        setModalOpen,
        modalOpenCreate,
        setModalOpenCreate,
        infoEmploy,
        setInfoEmploy,
        isEdit,
        setIsEdit,
        employ,
        setEmploy,
        state,
        dispatch,
        stateAreas,
        dispatchAreas,
        stateIdentifications,
        dispatchIdentifications,
      }}
    >
      {children}
    </CedinetContext.Provider>
  );
};
