import { ListEmployes } from './components/employes/ListEmployes';
import { IndexProvider } from './context/Index';

import './index.css';

const App = () => {
  return (
    <IndexProvider>
      <ListEmployes />
    </IndexProvider>
  );
};

export default App;
