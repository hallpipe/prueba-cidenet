import axios from 'axios';

export const getIdentifications = async () => {
  try {
    return await axios.get(`${process.env.REACT_APP_API_URL}/identificactions`);
  } catch (error) {
    return {
      error: true,
      data: null,
      msg: 'Ocurrio un error consultando los usuarios',
    };
  }
};

export const createIdentifications = async (body) => {
  try {
    return await axios.post(
      `${process.env.REACT_APP_API_URL}/identificactions/create`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  } catch (error) {
    throw error.response;
  }
};
