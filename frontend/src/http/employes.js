import axios from 'axios';

export const createEmployes = async (body) => {
  try {
    return await axios.post(
      `${process.env.REACT_APP_API_URL}/employes/create`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  } catch (error) {
    return {
      error: true,
      data: null,
      msg: 'Ocurrio un error consultando los usuarios',
    };
  }
};

export const editEmployes = async (id, body) => {
  try {
    return await axios.put(
      `${process.env.REACT_APP_API_URL}/employes/update/${id}`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  } catch (error) {
    return {
      error: true,
      data: null,
      msg: 'Ocurrio un error consultando los usuarios',
    };
  }
};

export const getEmployes = async () => {
  try {
    return await axios.get(`${process.env.REACT_APP_API_URL}/employes`);
  } catch (error) {
    return {
      error: true,
      data: null,
      msg: 'Ocurrio un error consultando los usuarios',
    };
  }
};

export const filterEmployes = async (body) => {
  try {
    return await axios.post(
      `${process.env.REACT_APP_API_URL}/employes/filter`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  } catch (error) {
    return {
      error: true,
      data: null,
      msg: 'Ocurrio un error consultando los usuarios',
    };
  }
};

export const deleteEmploy = async (id) => {
  try {
    return await axios.delete(
      `${process.env.REACT_APP_API_URL}/employes/delete/${id}`
    );
  } catch (error) {
    return {
      error: true,
      data: null,
      msg: 'Ocurrio un error eliminado el usuario los usuarios',
    };
  }
};
