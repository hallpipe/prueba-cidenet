import axios from 'axios';

export const getArea = async () => {
  try {
    return await axios.get(`${process.env.REACT_APP_API_URL}/area`);
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const createArea = async (body) => {
  try {
    return await axios.post(
      `${process.env.REACT_APP_API_URL}/area/create`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  } catch (error) {
    throw error.response;
  }
};
